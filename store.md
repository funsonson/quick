基本设置
--------

### 支付设置（stripe在线支付申请 & 配置）

>  Stripe和Paypal的价格和服务都差不多，费率稍微便宜一点；Stripe基本上支持大部分常用的卡；选择Stripe最主要的原因还是Stripe的申请速度非常快，很简单，审核方面也比较宽松。Stripe目前允许绑定个人卡。英国的费率是1.4% + 20p. 目前我们的网站系统无法支持Paypal。
   
>   Stripe的账号系统可以看到店主的钱。客户的支付马上就会显示在Stripe的系统里面，但是需要7天才能体现，这个不是自动，需要店主登陆到Stripe的网站里面取钱。这个还是很方便的，和Paypal的流程也是一样的。

1. [申请stripe账号视频教程](https://chinasoftware.co.uk/stripe%e8%b4%a6%e5%8f%b7%e7%9a%84%e7%94%b3%e8%af%b7%e5%92%8c%e6%bf%80%e6%b4%bb/)

2. Stripe通过审批后，再次登录stripe，在首页 Get your live API keys中，获取publishable key和Security Key。

    ![](images/store-02.png)

3. 将获取到的publishable key和Security Key 填入到系统，点击系统后台》系统配置》支付设置，填入后保存。

    ![](images/store-03.png)


### 网站设置

### 联系方式

### 营业时间设置



### 配送设置

### 提交google sitemap的方法

https://www.xxx.co.uk/site/sitemap 先访问这个生成xml文件

再访问 https://www.xxx.co.uk/sitemap.xml 或把它提交到google sitemap

