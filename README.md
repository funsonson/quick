快手点单系统功能
------

### 功能列表

|  基础版   | 白金版  | 钻石版  |
|  ----  | ----  |  ---- |
| 订单管理  | 订单管理 | 订单管理 |
| 订单云打印/后台监控/邮件监控  | 订单云打印/后台监控/邮件监控 | 订单云打印/后台监控/邮件监控 |
| 在线支付直接到个人银行账号  | 在线支付直接到个人银行账号 | 在线支付直接到个人银行账号 |
| 网站设置(营业时间&备餐时间等)  | 网站设置(营业时间&备餐时间等) | 网站设置(营业时间&备餐时间等) |
| 餐品&套餐/分类管理  | 餐品&套餐/分类管理 | 餐品&套餐/分类管理 |
| 会员管理(备注&黑名单等)  | 会员管理(备注&黑名单等) | 会员管理(备注&黑名单等) |
| 根据距离设置配送费  | 根据距离设置配送费 | 根据距离设置配送费 |
| 优惠券  | 优惠券 | 优惠券 |
| 礼品券(馈赠亲友)  | 礼品券(馈赠亲友) | 礼品券(馈赠亲友) |
| 全站餐品打折/阶梯打折  | 全站餐品打折/阶梯打折 | 全站餐品打折/阶梯打折 |
| -  | 在线订座(云打印&邮件提醒) | 在线订座(云打印&邮件提醒) |
| -  | - | 店内餐台自助扫码点餐 |


各部分功能请到子目录查看

* [餐品&套餐](product.md)
* [下单](checkout.md)
* [订单](order.md)
* [云打印机](printer.md)
* [优惠券&礼品券](coupon.md)
* [促销](promotion.md)
* [会员](user.md)
* [网站基本设置](store.md)

### 运营宝典
- [开源](income.md)
- [节流](outcome.md)

### 和在线平台对比

|  #   | 平台  | 系统  |
|  ----  | ----  |  ---- |
| 收款  | 由平台统一收款，扣除手续费、平台抽佣后择期返回给用户 | 用户用自己的银行卡申请stripe.com账号，钱直接到用户的银行卡，不经过第三方，仅由stripe收取手续费(1.4% + 0.2，一个30英镑订单到手29.38) |
| 品牌  | 在平台内入驻，容易引流到平台内其他店铺中 | 自有独立域名，单店，可以打印域名二维码 |
| 推广  | 在平台内竞价排位 | 可以自己选择推广方式，比如社交媒体facebook、twitter、instagram等，也可以传单线下推广 |
| 差评  | 需要时刻关注用户评论 | 可以自定义用户评论 |
| 餐价  | 找平台修改 | 自有后台，可以找我们或者自行修改 |
| 报税  | 和税务系统对接 | 你懂的 |
| 费用  | 平台抽佣，20+% | 域名是你自己的。网站下单系统30镑一个月，包括整理菜单上传。后台你可以自己管理。首次付一年360英镑帮建站，无其他额外建站费。第二年以后每月30英镑，可以月付年付 |
| 提醒  | 平台系统提醒 | 云打印机语音播报并打印出单小票，带自动切刀的160英镑，手撕的120英镑。另外提供页面语音播报、邮件通知、下发到收银点餐系统等通知方式 |



