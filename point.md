
消费积分 - 开发计划
----------------

用户在网站消费可以获得积分，用户通过积分兑换成优惠券，下单页面选择优惠券避免输入兑换积分值减少手机用户不方便操作。

- 可以设置开启不开启消费积分功能

- 每100积分固定对应1磅。可以设定每消费1英镑可以获得多少积分，默认消费1磅1积分。比如设定消费1英镑获得5积分，相当于每20磅获得100积分可兑换1磅

- 用户可以100积分兑1磅、500积分兑5磅、1000积分兑10磅三种兑换方式

- 用户登录后在醒目位置提示有多少积分，旁边有去兑换页面的链接

- 用户消费下单后，在结算成功页面立即显示新的积分值刺激用户

- 取消订单的客户，店主可以在后台修改用户的积分，如果情节刷单用户，可以将用户拉进黑名单

- 用户管理界面可以查看当前用户的积分，可以导出称excel，在excel中对用户进行群发邮件

- 历史消费记录如果需要单独联系管理员生成积分【只能做一次，不然会混乱】

> 现金订单也能获得积分，用户有通过现金订单刷单的可能，店主可以对刷单用户拉入黑名单，系统不承担用户刷单导致的损失。
